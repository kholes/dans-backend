# Api service dans test project

Api service

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development purposes.

### Prerequisites
Docker and docker-compose installed on your local machine.

### Running up in your local machine
Copy the `.env` to root in this working directory

and open your terminal in this working directory, and type
```
docker-compose up --build -d
```
Go to your browser and type `localhost`
