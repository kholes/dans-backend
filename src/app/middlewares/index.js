const jwt = require('jsonwebtoken')

module.exports = {
  auth: async (req, res, next) => {
    try {
      if (!req.headers || !req.headers.authorization) {
        res.status(401).json({ name: 'RequiredToken', message: 'Token is required.' })
      }
      const authorization = req.headers.authorization
      const token = authorization.split(' ')[1]
      const secret = process.env.SECRET_KEY
      const auth = await jwt.verify(token, secret) ?? null
      req.user = auth
      next()
    } catch (error) {
      res.status(401).json({ name: 'InvalidToken', message: 'Invalid Token.' })
    }
  }
}
