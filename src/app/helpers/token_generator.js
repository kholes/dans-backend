'use_strict'
const jwt = require('jsonwebtoken')

module.exports = {
  createToken: () => {
    const iat = Math.floor(new Date().getTime() / 1000)
    const secret = process.env.SECRET_KEY
    const duration = process.env.EXPIRED_KEY
    const access_payload = {
      iat: iat,
      exp: iat + Number(duration)
    }
    const token = {
      type: 'Bearer',
      expires_at: access_payload.exp,
      expires_in: access_payload.exp - access_payload.iat,
      access_token: jwt.sign(access_payload, secret)
    }

    return token
  }
}
