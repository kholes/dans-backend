const { users, jobs } = require('data/database/models')
const { createToken } = require('app/helpers/token_generator')
const { Op } = require('sequelize')
const { isArray } = require('lodash')

module.exports = {
  users: (req) => {
    return users.findAll()
  },
  addUser: (req) => {
    return users.create(req.body)
  },
  jobs: async (req) => {
    const description = req.query.description
    const location = req.query.location

    let filter = []
    description && filter.push(
      {
        description: {
          [Op.iLike]: `%${description}%`
        }
      },
      {
        name: {
          [Op.iLike]: `%${description}%`
        }
      }
    )

    location && filter.push(
      {
        location: {
          [Op.iLike]: `%${location}%`
        }
      }
    )

    filter = filter.length > 0 ? { [Op.or] : filter } : {}
    const page = req.query.page ? req.query.page : 1
    const per_page = 2
    const offset = +page * +per_page - +per_page
    const result = await jobs.findAndCountAll({
      where: filter, 
      limit: per_page, 
      offset: offset
    })
    return {
      meta: { 
        total: result.count, 
        page: +page, 
        total_page: Math.ceil(result.count / +per_page), 
        offset: offset
      },
      data: result.rows
    }
  },
  addJob: (req) => {
    return jobs.create(req.body)
  },
  login: async (req, res) => {
    if (req.body.username) {
      const userEntity = await users.findOne({where: {username: req.body.username}})
      if (userEntity && userEntity.password === req.body.password) {
        const token = await createToken()
        const data = {
          id: userEntity.id,
          username: userEntity.username,
          token: token
        }
        return data
      } else {
        res.status(400).json({
          name: "InvalidUser",
          message: "Invalid username or password"
        })
      }
    } else {
      res.status(400).json({
        name: "BadRequest",
        message: "Mandatory Parameters(s)"
      })
    }
  }
}