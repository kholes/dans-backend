const express = require('express')
const { auth } = require('../../app/middlewares')
const router = express.Router()

const { login, users, jobs, addUser, addJob } = require('../../app')
router.get('/', (req, res, next) => {
  res.status(200).json({
    title: process.env.APP_NAME,
    version: process.env.APP_VERSION
  })
})

router.get('/users', auth, async (req, res, next) => {
  try {
    const results = await users()
    res.status(200).json(results)
  } catch (e) {
    next(e)
  }
})

router.post('/users', auth, async (req, res, next) => {
  try {
    const results = await addUser(req)
    res.status(200).json(results)
  } catch (e) {
    next(e)
  }
})


router.get('/jobs', auth, async (req, res, next) => {
  try {
    const results = await jobs(req)
    res.status(200).json(results)
  } catch (e) {
    next(e)
  }
})

router.post('/jobs', auth, async (req, res, next) => {
  try {
    const results = await addJob(req)
    res.status(200).json(results)
  } catch (e) {
    next(e)
  }
})

router.post('/login', async (req, res, next) => {
  try {
    const result = await login(req, res)
    res.status(200).json(result)
  } catch (e) {
    next(e)
  }
})

module.exports = router
