module.exports = (sequelize, DataTypes) => {
  const jobs = sequelize.define('jobs', {
    name: {
      type: DataTypes.STRING
    },
    description: {
      type: DataTypes.STRING
    },
    location: {
      type: DataTypes.STRING
    },
    status: {
      type: DataTypes.STRING
    }
  })

  return jobs
}
