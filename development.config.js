module.exports = {
  apps: [{
    name: process.env.APP_NAME,
    script: 'src/server/bin/www',
    watch: true,
    ignore_watch: ['/app/node_modules', '/app/.postgres-data']
  }]
}
