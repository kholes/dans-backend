FROM node:14.15.0-alpine
WORKDIR /app
COPY ./wait-for .
RUN yarn global add pm2 sequelize-cli
RUN chmod +x ./wait-for
COPY ./package.json .
RUN yarn install
COPY . .